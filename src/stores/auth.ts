import type { User } from "@/types/User"
import { defineStore } from "pinia"
import { ref,computed } from "vue"

export const userAuthStore = defineStore('auth',()=> {
    const currentUser = ref<User>({
        id:1,
        email: 'tnc@gmail.com',
        password: '12345',
        fullName: 'tnc pop',
        gender: 'male',
        roles: ['user'],
    })
    return{ currentUser }
})