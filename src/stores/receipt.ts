import type { Product } from "@/types/Product";
import type { Receipt } from "@/types/Receipt";
import type { ReceiptItem } from "@/types/ReceiptItem";
import { defineStore } from "pinia";
import { ref } from "vue";
import { userAuthStore } from "./auth";
import { useMemberStore } from "./member";


export const userReceiptStore =defineStore('receipt',() => {
    const authStroe =userAuthStore()
    const memberStroe = useMemberStore()
    const receiptDialog = ref(false)
    const receipt =ref<Receipt>({
    id: 0,
    createDate: new Date(),
    totalBefor: 0,
    total: 0,
    receivedAmount: 0,
    paymentType: "cash",
    userId: authStroe.currentUser.id,
    user: authStroe.currentUser,
    memberId: 0,
    memberDiscount: 0
})
    const receiptItem = ref<ReceiptItem[]>([])
    function addReceiptItem(product: Product) {
       const index = receiptItem.value.findIndex((item)=> item.product?.id===product.id)
        if(index >=0){
            receiptItem.value[index].unit++
            calReceipt()
        }else{
            const newReceipt: ReceiptItem = {
            id: -1,
            name: product.name,
            price: product.price,
            unit: 1,
            productId: product.id,
            product: product
        }
        receiptItem.value.push(newReceipt)
        calReceipt()
    }
        
    } 
    function removeReceiptItem(receiptitem: ReceiptItem){
        const index = receiptItem.value.findIndex((item) => item === receiptitem) 
        receiptItem.value.splice(index,1)
        calReceipt()
    }
    function inc(item: ReceiptItem) {
        item.unit++
        calReceipt()
    }
    function dec(item: ReceiptItem){
        if(item.unit===1){
            removeReceiptItem(item)
        }
        item.unit--
        calReceipt()
    }
    function calReceipt() {
        let totalBefore =0
        for(const item of receiptItem.value) {
            totalBefore = totalBefore+(item.price*item.unit)
       }
       if(memberStroe.currentMember)
       receipt.value.totalBefor = totalBefore *0.95
       else{ receipt.value.totalBefor = totalBefore}
    }
    function showReceiptDialog() {
        receiptDialog.value=true
        receipt.value.receiptItems = receiptItem.value

    }
    function clear() {
        receiptItem.value = []
        receipt.value = {
            
                id: 0,
                createDate: new Date(),
                totalBefor: 0,
                total: 0,
                receivedAmount: 0,
                paymentType: "cash",
                userId: authStroe.currentUser.id,
                user: authStroe.currentUser,
                memberId: 0,
                memberDiscount: 0

        }
        memberStroe.clear()
    }
    return {
        receiptItem, receipt,receiptDialog,
        addReceiptItem,removeReceiptItem,inc,dec,calReceipt,showReceiptDialog,clear
    }
})