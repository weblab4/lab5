import type { Member } from "./Member";
import type { ReceiptItem } from "./ReceiptItem";
import type { User } from "./User";


type Receipt = {
    id: number;
    createDate: Date;
    totalBefor:number;
    total: number;
    receivedAmount: number;
    paymentType: string;
    userId: number;
    user?: User;
    memberId: number;
    member?: Member;
    memberDiscount: number;
    receiptItems?: ReceiptItem[]
    }
    export type {Receipt}